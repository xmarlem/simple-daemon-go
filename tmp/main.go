package main

import (
  "encoding/json"
  "fmt"
  "strings"
)

type Persona struct {
  Nome string `json:"nome"`
}

func main() {

  var p Persona
  //m := &Persona{}
  r := strings.NewReader("{\"nome\": \"Marco\"}")

  dec := json.NewDecoder(r)
  err := dec.Decode(&p)
  if err != nil {
    panic(err)
  }
  fmt.Println(p.Nome)

}
